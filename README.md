# http-status-codes

[![pipeline status](https://gitlab.com/simonbreiter/http-status-codes/badges/main/pipeline.svg)](https://gitlab.com/simonbreiter/http-status-codes/-/commits/main)
[![coverage report](https://gitlab.com/simonbreiter/http-status-codes/badges/main/coverage.svg)](https://gitlab.com/simonbreiter/http-status-codes/-/commits/main)

Repository of https://codes.simonbreiter.com. An API for testing HTTP status codes. 

## Local development

Install [mise](https://mise.jdx.dev/getting-started.html) and [Docker](https://docs.docker.com/get-docker/). Run `mise install` to install the correct version of Node.js and `npm install` to install dependencies and setup Git Hooks. Run `npm run dev` or if you want to use containers `npm run docker:dev` and open http://localhost:3000. Server restarts on code change.

## Git Hooks

To ensure basic quality control and a consistent codebase, a few checks are made on every commit by [husky](https://github.com/typicode/husky).

### pre-commit

[Prettier](https://prettier.io) formats staged code. [Jest](https://jestjs.io) is used for testing.

### commit-msg

All commit messages are linted with [commitlint](https://commitlint.js.org/#/) and have to follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) specification.
