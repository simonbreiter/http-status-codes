ARG NODE_VERSION
FROM node:${NODE_VERSION}-alpine as base
WORKDIR /home/node/app
COPY --chown=node:node src src
COPY --chown=node:node package.json package-lock.json ./
RUN npm ci --omit=dev --ignore-scripts
USER node

ARG COMMIT_SHORT_SHA
FROM base as prod
ENV COMMIT_SHORT_SHA=${COMMIT_SHORT_SHA}
COPY --chown=node:node dist dist
EXPOSE 3000
CMD ["npm", "run", "start"]

FROM base as dev
COPY --chown=node:node package.json tsconfig.json ./
RUN npm install --no-audit
EXPOSE 3000 9229
CMD ["npm", "run", "dev"]