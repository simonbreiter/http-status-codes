const model: any = {
  '200': {
    status: 'OK',
    description:
      'Standard response for successful HTTP requests. The actual response will depend on the request method used. In a GET request, the response will contain an entity corresponding to the requested resource. In a POST request, the response will contain an entity describing or containing the result of the action.'
  },
  '201': {
    status: 'Created',
    description:
      'The request has been fulfilled, resulting in the creation of a new resource.'
  },
  '202': {
    status: 'Accepted',
    description:
      'The request has been accepted for processing, but the processing has not been completed. The request might or might not be eventually acted upon, and may be disallowed when processing occurs.'
  },
  '203': {
    status: 'Non-Authoritative Information',
    description:
      "The server is a transforming proxy (e.g. a Web accelerator) that received a 200 OK from its origin, but is returning a modified version of the origin's response."
  },
  '204': {
    status: 'No Content',
    description:
      'The server successfully processed the request, and is not returning any content.'
  },
  '205': {
    status: 'Reset Content',
    description:
      'The server successfully processed the request, asks that the requester reset its document view, and is not returning any content.'
  },
  '206': {
    status: 'Partial Content',
    description:
      'The server is delivering only part of the resource (byte serving) due to a range header sent by the client. The range header is used by HTTP clients to enable resuming of interrupted downloads, or split a download into multiple simultaneous streams.'
  },
  '207': {
    status: 'Multi-Status',
    description:
      'The message body that follows is by default an XML message and can contain a number of separate response codes, depending on how many sub-requests were made.'
  },
  '208': {
    status: 'Already Reported',
    description:
      'The members of a DAV binding have already been enumerated in a preceding part of the (multistatus) response, and are not being included again.'
  },
  '226': {
    status: 'IM Used',
    description:
      'The server has fulfilled a request for the resource, and the response is a representation of the result of one or more instance-manipulations applied to the current instance.'
  },
  '300': {
    status: 'Multiple Choices',
    description:
      'Indicates multiple options for the resource from which the client may choose (via agent-driven content negotiation). For example, this code could be used to present multiple video format options, to list files with different filename extensions, or to suggest word-sense disambiguation.'
  },
  '301': {
    status: 'Moved Permanently',
    description:
      'This and all future requests should be directed to the given URI.'
  },
  '302': {
    status: 'Found',
    description:
      'Tells the client to look at (browse to) another URL. The HTTP/1.0 specification (RFC 1945) required the client to perform a temporary redirect with the same method (the original describing phrase was "Moved Temporarily"),[21] but popular browsers implemented 302 redirects by changing the method to GET. Therefore, HTTP/1.1 added status codes 303 and 307 to distinguish between the two behaviours.'
  },
  '303': {
    status: 'See Other',
    description:
      'The response to the request can be found under another URI using the GET method. When received in response to a POST (or PUT/DELETE), the client should presume that the server has received the data and should issue a new GET request to the given URI.'
  },
  '304': {
    status: 'Not Modified',
    description:
      'Indicates that the resource has not been modified since the version specified by the request headers If-Modified-Since or If-None-Match. In such case, there is no need to retransmit the resource since the client still has a previously-downloaded copy.'
  },
  '305': {
    status: 'Use Proxy',
    description:
      'The requested resource is available only through a proxy, the address for which is provided in the response. For security reasons, many HTTP clients (such as Mozilla Firefox and Internet Explorer) do not obey this status code.'
  },
  '306': {
    status: 'Switch Proxy',
    description:
      'No longer used. Originally meant "Subsequent requests should use the specified proxy."'
  },
  '307': {
    status: 'Temporary Redirect',
    description:
      'In this case, the request should be repeated with another URI; however, future requests should still use the original URI. In contrast to how 302 was historically implemented, the request method is not allowed to be changed when reissuing the original request. For example, a POST request should be repeated using another POST request.'
  },
  '308': {
    status: 'Permanent Redirect',
    description:
      'This and all future requests should be directed to the given URI. 308 parallel the behaviour of 301, but does not allow the HTTP method to change. So, for example, submitting a form to a permanently redirected resource may continue smoothly.'
  },
  '400': {
    status: 'Bad Request',
    description:
      'The server cannot or will not process the request due to an apparent client error (e.g., malformed request syntax, size too large, invalid request message framing, or deceptive request routing).'
  },
  '401': {
    status: 'Unauthorized',
    description:
      'Similar to 403 Forbidden, but specifically for use when authentication is required and has failed or has not yet been provided. The response must include a WWW-Authenticate header field containing a challenge applicable to the requested resource. See Basic access authentication and Digest access authentication. 401 semantically means "unauthorised", the user does not have valid authentication credentials for the target resource.'
  },
  '402': {
    status: 'Payment Required',
    description:
      'Reserved for future use. The original intention was that this code might be used as part of some form of digital cash or micropayment scheme, as proposed, for example, by GNU Taler, but that has not yet happened, and this code is not widely used. Google Developers API uses this status if a particular developer has exceeded the daily limit on requests. Sipgate uses this code if an account does not have sufficient funds to start a call. Shopify uses this code when the store has not paid their fees and is temporarily disabled. Stripe uses this code for failed payments where parameters were correct, for example blocked fraudulent payments.'
  },
  '403': {
    status: 'Forbidden',
    description:
      'The request contained valid data and was understood by the server, but the server is refusing action. This may be due to the user not having the necessary permissions for a resource or needing an account of some sort, or attempting a prohibited action (e.g. creating a duplicate record where only one is allowed). This code is also typically used if the request provided authentication by answering the WWW-Authenticate header field challenge, but the server did not accept that authentication. The request should not be repeated.'
  },
  '404': {
    status: 'Not Found',
    description:
      'The requested resource could not be found but may be available in the future. Subsequent requests by the client are permissible.'
  },
  '405': {
    status: 'Method Not Allowed',
    description:
      'A request method is not supported for the requested resource; for example, a GET request on a form that requires data to be presented via POST, or a PUT request on a read-only resource.'
  },
  '406': {
    status: 'Not Acceptable',
    description:
      'The requested resource is capable of generating only content not acceptable according to the Accept headers sent in the request.'
  },
  '407': {
    status: 'Proxy Authentication Required',
    description: 'The client must first authenticate itself with the proxy.'
  },
  '408': {
    status: 'Request Timeout',
    description:
      'The server timed out waiting for the request. According to HTTP specifications: "The client did not produce a request within the time that the server was prepared to wait. The client MAY repeat the request without modifications at any later time."'
  },
  '409': {
    status: 'Conflict',
    description:
      'Indicates that the request could not be processed because of conflict in the current state of the resource, such as an edit conflict between multiple simultaneous updates.'
  },
  '410': {
    status: 'Gone',
    description:
      'Indicates that the resource requested is no longer available and will not be available again. This should be used when a resource has been intentionally removed and the resource should be purged. Upon receiving a 410 status code, the client should not request the resource in the future. Clients such as search engines should remove the resource from their indices. Most use cases do not require clients and search engines to purge the resource, and a "404 Not Found" may be used instead.'
  },
  '411': {
    status: 'Length Required',
    description:
      'The request did not specify the length of its content, which is required by the requested resource.'
  },
  '412': {
    status: 'Precondition Failed',
    description:
      'The server does not meet one of the preconditions that the requester put on the request header fields.'
  },
  '413': {
    status: 'Payload Too Large',
    description:
      'The request is larger than the server is willing or able to process. Previously called "Request Entity Too Large".'
  },
  '414': {
    status: 'URI Too Long',
    description:
      'The URI provided was too long for the server to process. Often the result of too much data being encoded as a query-string of a GET request, in which case it should be converted to a POST request. Called "Request-URI Too Long" previously.'
  },
  '415': {
    status: 'Unsupported Media Type',
    description:
      'The request entity has a media type which the server or resource does not support. For example, the client uploads an image as image/svg+xml, but the server requires that images use a different format.'
  },
  '416': {
    status: 'Range Not Satisfiable',
    description:
      'The client has asked for a portion of the file (byte serving), but the server cannot supply that portion. For example, if the client asked for a part of the file that lies beyond the end of the file. Called "Requested Range Not Satisfiable" previously.'
  },
  '417': {
    status: 'Expectation Failed',
    description:
      'The server cannot meet the requirements of the Expect request-header field.'
  },
  '418': {
    status: "I'm a teapot",
    description:
      "This code was defined in 1998 as one of the traditional IETF April Fools' jokes, in RFC 2324, Hyper Text Coffee Pot Control Protocol, and is not expected to be implemented by actual HTTP servers. The RFC specifies this code should be returned by teapots requested to brew coffee. This HTTP status is used as an Easter egg in some websites, such as Google.com's I'm a teapot easter egg."
  },
  '421': {
    status: 'Misdirected Request',
    description:
      'The request was directed at a server that is not able to produce a response (for example because of connection reuse).'
  },
  '422': {
    status: 'Unprocessable Entity',
    description:
      'The request was well-formed but was unable to be followed due to semantic errors.'
  },
  '423': {
    status: 'Locked',
    description: 'The resource that is being accessed is locked.'
  },
  '424': {
    status: 'Failed Dependency',
    description:
      'The request failed because it depended on another request and that request failed (e.g., a PROPPATCH).'
  },
  '425': {
    status: 'Too Early',
    description:
      'Indicates that the server is unwilling to risk processing a request that might be replayed.'
  },
  '426': {
    status: 'Upgrade Required',
    description:
      'The client should switch to a different protocol such as TLS/1.3, given in the Upgrade header field.'
  },
  '428': {
    status: 'Precondition Required',
    description:
      "The origin server requires the request to be conditional. Intended to prevent the 'lost update' problem, where a client GETs a resource's state, modifies it, and PUTs it back to the server, when meanwhile a third party has modified the state on the server, leading to a conflict."
  },
  '429': {
    status: 'Too Many Requests',
    description:
      'The user has sent too many requests in a given amount of time. Intended for use with rate-limiting schemes.'
  },
  '431': {
    status: 'Request Header Fields Too Large',
    description:
      'The server is unwilling to process the request because either an individual header field, or all the header fields collectively, are too large.'
  },
  '451': {
    status: 'Unavailable For Legal Reasons',
    description:
      'A server operator has received a legal demand to deny access to a resource or to a set of resources that includes the requested resource. The code 451 was chosen as a reference to the novel Fahrenheit 451.'
  },
  '500': {
    status: 'Internal Server Error',
    description:
      'A generic error message, given when an unexpected condition was encountered and no more specific message is suitable.'
  },
  '501': {
    status: 'Not Implemented',
    description:
      'The server either does not recognize the request method, or it lacks the ability to fulfil the request. Usually this implies future availability (e.g., a new feature of a web-service API).'
  },
  '502': {
    status: 'Bad Gateway',
    description:
      'The server was acting as a gateway or proxy and received an invalid response from the upstream server.'
  },
  '503': {
    status: 'Service Unavailable',
    description:
      'The server cannot handle the request (because it is overloaded or down for maintenance). Generally, this is a temporary state.'
  },
  '504': {
    status: 'Gateway Timeout',
    description:
      'The server was acting as a gateway or proxy and did not receive a timely response from the upstream server.'
  },
  '505': {
    status: 'HTTP Version Not Supported',
    description:
      'The server does not support the HTTP protocol version used in the request.'
  },
  '506': {
    status: 'Variant Also Negotiates',
    description:
      'Transparent content negotiation for the request results in a circular reference.'
  },
  '507': {
    status: 'Insufficient Storage',
    description:
      'The server is unable to store the representation needed to complete the request.'
  },
  '508': {
    status: 'Loop Detected',
    description:
      'The server detected an infinite loop while processing the request (sent instead of 208 Already Reported).'
  },
  '510': {
    status: 'Not Extended',
    description:
      'Further extensions to the request are required for the server to fulfil it.'
  },
  '511': {
    status: 'Network Authentication Required',
    description:
      'The client needs to authenticate to gain network access. Intended for use by intercepting proxies used to control access to the network (e.g., "captive portals" used to require agreement to Terms of Service before granting full Internet access via a Wi-Fi hotspot).'
  }
}

export default model
