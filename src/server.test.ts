import request from 'supertest'
import server from './server'
import model from './model'

test('index', async () => {
  const response = await request(server).get('/')
  const co = Object.keys(model).map(code => {
    return {
      code: code,
      ...model[code]
    }
  })

  expect(response.headers['content-type']).toMatch(/json/)
  expect(response.status).toEqual(200)
  expect(response.body).toEqual(co)
})

test('status codes', async () => {
  const codeBlacklist = ['204', '304'] // Returns no content, has to be tested in another test
  const filteredCodes = Object.keys(model).filter(
    code => !codeBlacklist.includes(code.toString())
  )

  for (const code of filteredCodes) {
    const response = await request(server).get(`/${code}`)

    const expected = {
      code: code.toString(),
      ...model[code]
    }

    expect(response.headers['content-type']).toMatch(/json/)
    expect(response.status).toEqual(parseInt(code))
    expect(response.body).toEqual(expected)
  }
})

test('status codes with no content', async () => {
  const codeBlacklist = ['204', '304']

  for (const code of codeBlacklist) {
    const response = await request(server).get(`/${code}`)

    expect(response.status).toEqual(parseInt(code))
  }
})
