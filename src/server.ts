import express, { Express, Request, Response } from 'express'
import model from './model'

const server = express()

server.set('json spaces', 2)

server.get('/api/healthz', (req: Request, res: Response) => {
  res.status(200).send('OK')
})

server.get('/api/readiness', (req: Request, res: Response) => {
  res.status(200).send('OK')
})

server.get('/api/version', (req: Request, res: Response) => {
  const commitShortSha = process.env.COMMIT_SHORT_SHA || 'unknown'
  res.status(200).json({ version: commitShortSha })
})

server.get('/', async (req: Request, res: Response) => {
  res.status(200)
  const co = Object.keys(model).map(code => {
    return {
      code: code,
      ...model[code]
    }
  })
  res.send(co)
})

server.get('/:code', async (req: Request, res: Response) => {
  const codes = Object.keys(model)
    .filter(codeToFilter => codeToFilter !== '')
    .map(codeToMap => parseInt(codeToMap))
  const code = parseInt(req.params.code)

  if (codes.includes(code)) {
    res.status(code)
    res.json({
      code: code.toString(),
      ...model[code]
    })
  } else {
    res.status(404)
    res.json({ message: `${code} not found` })
  }
})

export default server
